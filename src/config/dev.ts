export const folder = {
    f_send: '/tesroot/comdata/TWMSKJSON/out',
    f_error: '/tesroot/comdata/MSK_TRADELENS/error',
    f_res: '/tesroot/comdata/MSK_TRADELENS/in',  
}

export let pathApi: any = {
    E217: '/api/v1/transportEvents/actualGateIn',
    E268: '/api/v1/transportEvents/actualGateOut',
    E452: '/api/v1/transportEvents/actualLoadedOnVessel',
    E453: '/api/v1/transportEvents/actualDischargeFromVessel',
    E280: '/api/v1/transportEvents/actualVesselArrival',
    E025: '/api/v1/transportEvents/actualVesselDeparture',
    E500: '/api/v1/transportEvents/estimatedVesselDeparture',
    E501: '/api/v1/transportEvents/estimatedVesselArrival',
    E472: '/api/v1/genericEvents/cutOffTime',
    E292: '/api/v1/genericEvents/customsRelease',
    E425: '/api/v1/genericEvents/packedContainerReadyToLoad',
    E429: '/api/v1/genericEvents/terminalRelease',
    E336: '/api/v1/genericEvents/shiftPier',
    E335: '/api/v1/genericEvents/shiftCell',
}

/*

## Folder for send
FOLDER_API_12_EVENTS_SEND=/tesroot/comdata/TWMSKJSON/out
FOLDER_API_12_EVENTS_ERROR=/tesroot/comdata/MSK_TRADELENS/error
FOLDER_API_12_EVENTS_RESPONSE=/tesroot/comdata/MSK_TRADELENS/in

## - 1 - Actual gate in (E217)
ACTUAL_GATE_IN=/api/v1/transportEvents/actualGateIn

## - 2 - Actual gate out (E268)
ACTUAL_GATE_OUT=/api/v1/transportEvents/actualGateOut

## - 3 - Actual loaded on vessel (E452)
ACTUAL_LOADED_ON_VESSEL=/api/v1/transportEvents/actualLoadedOnVessel

## - 4 - Actual discharge from vessel (E453)
ACTUAL_DISCHARGE_FROM_VESSEL=/api/v1/transportEvents/actualDischargeFromVessel

## - 5 - Actual vessel arrival (E280)
ACTUAL_VESSEL_ARRIVAL=/api/v1/transportEvents/actualVesselArrival

## - 6 - Actual vessel departure (E025)
ACTUAL_VESSEL_DEPARTURE=/api/v1/transportEvents/actualVesselDeparture

## - 7 - Estimated vessel departure (E500)
ESTIMATED_VESSEL_DEPARTURE=/api/v1/transportEvents/estimatedVesselDeparture

## - 8 - Estimated vessel arrival (E501)
ESTIMATED_VESSEL_ARRIVAL=/api/v1/transportEvents/estimatedVesselArrival

## - 9 - Cut Off Time (E472)
CUT_OFF_TIME=/api/v1/genericEvents/cutOffTime

## - 10 - Customs release (E292)
CUSTOM_RELEASE=/api/v1/genericEvents/customsRelease

## - 11 - Packed transport equipment ready to load (E425)
PACKED_CNTR_READY_TO_LOAD=/api/v1/genericEvents/packedContainerReadyToLoad

## - 12 - Terminal release (E429)
TERMINAL_RELEASE=/api/v1/genericEvents/terminalRelease

*/