import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import dotenv from 'dotenv'
import fs from 'fs'

if (fs.existsSync('.env')) {
    dotenv.config({ path: '.env' })
} else {
    dotenv.config({ path: '.env.dev' })  // you can delete this after you create your own .env file!
}

import { runProcess } from './routes/api_msk'
const PORT = process.env['PORT']

const tipcApi = new Koa
tipcApi
    .use(bodyParser())
    .listen(PORT, () => {
        console.log(`The server is running on ${PORT} !!! Press Ctrl + C to stop server !!!`)
        runProcess()
    })