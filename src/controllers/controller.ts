import fs from 'fs'
import pathStr from 'path'
import _ from 'underscore'
import moment from 'moment'

import { STATUS } from '../types/cls_enum'
import MailController from './mail'
import { checkJson } from './valid'

import log from './logs'
import { folder, pathApi } from '../config/dev'

const request = require("request-promise")
const fsEx = require('fs-extra')

class TIPCController {
    public logger: log
    private cntSucess: number
    private cntError: number
    private stop:boolean
    public resultLog: log
    public error401:boolean

    constructor() {
        this.logger = new log('Tradelens')
        this.cntSucess = 0
        this.cntError = 0
        this.stop = false
        this.resultLog = new log('Result')
        this.error401 = false
    }

    lastProcess(){
        let str = `\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] ---------- Last time result - Success : [${this.cntSucess}], Error : [${this.cntError}]\n`
        this.cntError = 0
        this.cntSucess = 0
        this.resultLog.writeLog(str)
        return this.stop
    }

    async countFiles(){
        let path = folder.f_send
        // Sort list
        let files = fs.readdirSync(path);
        await files.sort(function (a, b) {
            return fs.statSync(pathStr.join(path, a)).mtimeMs - fs.statSync(pathStr.join(path, b)).mtimeMs
        });
        // Get 30 oldest files
        return _.toArray(files).slice(0, 30)
    }

    async sendFromOneFolder(token: string, list: string[]) {
        await Promise.all(
            list.map(file => {
                this.sendFile(file, token)
            })  
        )  
    }

    async sendFile(file: string, token: string) {

        let logInfo = `--------- Start send input JSON file: ${folder.f_send}/${file} ---------\n`
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Check validation data format \n`
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Process send data JSON into MSK API \n`

        let readData = ''
        try {
            readData = fs.readFileSync(`${folder.f_send}/${file}`, 'utf8')
            if (readData === "Stop") {
                return this.stop = true
            }
        } catch {
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: Can't read file \n`
            this.moveToErr(logInfo, file)
        }

        let _this = this
        try {
            const data = JSON.parse(readData)
            // Valid json
            let checkData = await checkJson(data, data.EventID)
            if (checkData.code === STATUS.WARNING || checkData.code === STATUS.ERROR) {
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: ${JSON.stringify(checkData.messeage)} \n`
                this.moveToErr(logInfo, file)
            } else {
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Check validation is OK \n`

                // Need remove : EventID, BaseLocation, Address, GeoCoordinates (class name) from JSON
                let replaceData = JSON.stringify(checkData.messeage)
                    .replace(data.EventID, '')
                    .replace('BaseLocation', '')
                    .replace('Address', '')
                    .replace('GeoCoordinates', '')

                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Post ${replaceData} to ${process.env['HOST']}${pathApi[data.EventID]} \n`

                request({
                    method: 'POST',
                    url: `${process.env['HOST']}${pathApi[data.EventID]}`,
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },
                    body: replaceData, 
                })
                    .then(async function (body: any) {
                        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data into ${body} \n`
                        let date = moment().format('YYYYMMDDhhmmssSSS')
                        let folder_date = moment().format('YYYYMMDD')
                        fs.writeFileSync(`${folder.f_res}/${folder_date}/${data.EventID}.${data.equipmentNumber}.${date}`, body)
                        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data into ${folder.f_res}/${folder_date}/${data.EventID}.${data.equipmentNumber}.${date} \n`
                        _this.deleteFile(logInfo, file)
                    })
                    .catch(async function (err: any) {
                        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: ${err} \n`
                        
                        if (err.statusCode) _this.moveToErr(logInfo, file, err.statusCode)
                        else _this.moveToErr(logInfo, file)
                    })
            }

        } catch {
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: Post data failed \n`
            this.moveToErr(logInfo, file)
        }
    }

    deleteFile(logInfo: string, file: any){
        this.cntSucess++
        fs.unlink(`${folder.f_send}/${file}`, (err) => {
            if (err) {
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: ${err} \n`
                logInfo += `--------- Finish : ${folder.f_send}/${file} ---------\n`
                return this.logger.writeLog(logInfo)
            }
            // Move file to error folder
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - File has been deleted ! \n`
            logInfo += `--------- Finish : ${folder.f_send}/${file} ---------\n`
            return this.logger.writeLog(logInfo)
        })
    }

    moveToErr(logInfo: string, file: any, errSts:any="error"){
        this.cntError++
        
        if (errSts == 401) {
            this.error401 = true;
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - The token is expired. The system will resend ! \n`
            logInfo += `--------- Finish : ${folder.f_send}/${file} ---------\n`
            return this.logger.writeLog(logInfo)            
        } else {
            let folder_date = moment().format('YYYYMMDD')
            fsEx.move(`${folder.f_send}/${file}`, `${folder.f_error}/${folder_date}/${file}`, { overwrite: true }, (err: any) => {                
                // Move file to error folder
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ${file} has moved to /MSK_TRADELENS/error/${folder_date}/ ! \n`
                logInfo += `--------- Finish : ${folder.f_send}/${file} ---------\n`
                return this.logger.writeLog(logInfo)
            })
        }        
    }

}

export default TIPCController