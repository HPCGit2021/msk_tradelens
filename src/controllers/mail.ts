import '../config/secrets'
import _ from 'underscore'

const nodemailer = require('nodemailer')

class MailController {
    private transporter: any
    private mailOptions: any
    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env['EMAIL_USER'],
                pass: process.env['EMAIL_PASS']
            }
        })
    }

    setContent(subject: string, content: string, to: string, attach: any) {
        this.mailOptions = {
            from: process.env['EMAIL_USER'],
            to: to,
            subject: subject,
            text: content,
            attachments: attach
        }
    }

    send() {
        this.transporter.sendMail(this.mailOptions, function (error: any, info: any) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        })
    }

}

export default MailController