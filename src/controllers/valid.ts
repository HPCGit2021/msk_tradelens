import { validate } from "class-validator"
import { E217 } from '../types/issueE217_event'
import { E268 } from '../types/issueE268_event' // publishActualGateOut_event at sandbox-tradelens
import { E452 } from '../types/issueE452_event'
import { E453 } from '../types/issueE453_event'
// import { E280 } from '../types/issueE280_event'
// import { E025 } from '../types/issueE025_event'
// import { E500 } from '../types/issueE500_event'
// import { E501 } from '../types/issueE501_event'
import { E472 } from '../types/issueE472_event'
import { E292 } from '../types/issueE292_event'
// import { E425 } from '../types/issueE425_event'
import { E429 } from '../types/issueE429_event'
import { E336 } from '../types/issueE336_event'
import { E335 } from '../types/issueE335_event'

import { EVENT } from '../types/cls_enum'

const eventType = {
    "E217": E217,
    "E268": E268,
    "E452": E452,
    "E453": E453,
    // "E280": E280,
    // "E025": E025,
    // "E500": E500,
    // "E501": E501,
    "E472": E472,
    "E292": E292,
    // "E425": E425,
    "E429": E429,
    "E336": E336,
    "E335": E335,
}

export function checkJson(json: string, type: EVENT) {

    if (type in EVENT) {
        return valid(new eventType[type](json))
    } else {
        return { code: 500, messeage: { errors: 'Unknown event ID' } }
    }

}

function valid(post: any) {
    return validate(post).then(errors => { // errors is an array of validation errors
        if (errors.length > 0) {
            let store: any = {}
            getErrors(errors, store)
            return { code: 400, messeage: store }
        } else {
            return { code: 200, messeage: post }
        }
    })
}

function getErrors(errors: any, store: any) {
    let l = errors.length
    for (let i = 0; i < l; i++) {
        if (errors[i].children && errors[i].children.length > 0) {
            store[errors[i].property] = {}
            getErrors(errors[i].children, store[errors[i].property])
        }
        else store[errors[i].property] = errors[i].constraints
    }
}