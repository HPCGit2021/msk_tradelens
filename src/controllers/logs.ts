import fs from 'fs'
import schedule from 'node-schedule'
import moment from 'moment'

class CustomerLog {
    private path: string
    private fileName: string
    private type: string

    constructor(type: string){
        this.path = './logs/'
        this.fileName = ''
        this.type = type
        this.createLog()
    }

    private createLog(){
        fs.mkdirSync(this.path, { recursive: true })
        this.createFile()
        const newFileName = schedule.scheduleJob('0 0 * * *', async() => {
            this.createFile()
        })
    }

    createFile(){
        let folder_date = moment().format('YYYYMMDD')
        fs.mkdirSync(`/tesroot/comdata/MSK_TRADELENS/in/${folder_date}`, { recursive: true })
        fs.mkdirSync(`/tesroot/comdata/MSK_TRADELENS/error/${folder_date}`, { recursive: true })

        this.fileName = `${this.path}${this.type}_${folder_date}.log`
        if (!fs.existsSync(this.fileName)) fs.writeFileSync(this.fileName, '--------- Have a good day ---------\n')
    }

    writeLog(str: any){
        if( str instanceof String ) str = str.toString()
        if( typeof str != "string" ) str = JSON.stringify( str )
        const buf = Buffer.from(str, 'utf8')

        let fd;
        try {
            fd = fs.openSync(this.fileName, 'a');
            fs.appendFileSync(fd, str, 'utf8');
        } catch (err) {
            /* Handle the error */
        } finally {
        if (fd !== undefined)
            fs.closeSync(fd);
        }
    }
}

export default CustomerLog
