import _ from 'underscore'
import moment from 'moment'

const request = require("request-promise");

class TokenController {

    getAccessToken() {
        return request({
            method: 'POST',
            url: `${process.env['TOKEN_HOST']}`,
            headers: {
                'Content-Type': 'application/x-www-formurlencoded'
            },
            form: {
                'grant_type': 'urn:ibm:params:oauth:grant-type:apikey',
                'apikey': process.env['API_KEY']
            }
        })
            .then(function (body: any) {
                // POST succeeded...
                console.log(`Access token has created at ${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}`)
                return body
            })
            .catch(function (err: any) {
                // POST failed...
                return ''
            })
    }

    getBearerToken(accessToken: any) {
        return request({
            method: 'POST',
            url: `${process.env['BEARER_HOST']}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: accessToken
        })
            .then(function (body: any) {
                // POST succeeded...
                console.log(`Bearer token has created at ${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}`)
                return JSON.parse(body).solution_token
            })
            .catch(function (err: any) {
                // POST failed...
                return ''
            })
    }

}

export default TokenController