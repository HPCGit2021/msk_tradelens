import {
    IsEnum,
    IsOptional,
    IsISO8601,
    IsString,
    IsNotEmpty,
} from 'class-validator'

import { IssueEvent } from './issue_event'
import { TRANSTYPE, FULLSTS } from './cls_enum'

export class E217 extends IssueEvent{

    @IsNotEmpty()
    @IsISO8601()
    eventOccurrenceTime8601: string // mandatory
    // example: 2017 - 09 - 15T15: 30: 00.000 - 05: 00
    // When the event occurred, in common ISO 8601 format yyyy - MM - dd'T'HH: mm: ss.SSSZ

    @IsNotEmpty()
    @IsEnum(TRANSTYPE)
    transportationPhase: TRANSTYPE
    // example: Import
    // Transportation phase indicates whether an event occurs during the export, transshipment, or import phase relative to a given consignment.The distinction is based on the ocean legs of the consignment's transport plan. Thus, "export" relates to any events occurring up until and including the vessel departure of the first ocean leg. "Import" relates to last vessel arrival and all subsequent events. All events which may occur in between export and import are marked as "transshipment". If the transport plan origin and destination countries are the same, all events are marked "domestic".

    @IsNotEmpty()
    @IsString()
    terminal: string
    // example: USNYC - MSK
    // UNLOCODE - Terminal Code(including hyphen in between).This is the full SMDG code.

    @IsNotEmpty()
    @IsEnum(FULLSTS)
    fullStatus: FULLSTS

    // ---------- Condition
    @IsOptional()
    @IsString()
    vehicleId: string
    // example: 233 - 005
    // ID of truck, rail, barge, or vessel

    @IsOptional()
    @IsString()
    vehicleName: string
    // example: Name of vehicle
    // Name of truck, rail, barge, or vessel

    @IsOptional()
    @IsString()
    voyageId: string
    // example: VOY83461
    // Voyage number

    constructor(json: any) {
        super(json)
        this.eventOccurrenceTime8601 = json.eventOccurrenceTime8601
        this.transportationPhase = json.transportationPhase
        this.terminal = json.terminal
        this.fullStatus = json.fullStatus
        this.vehicleId = json.vehicleId
        this.vehicleName = json.vehicleName
        this.voyageId = json.voyageId
    }

}