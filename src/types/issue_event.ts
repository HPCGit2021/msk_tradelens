import {
    IsInt,
    IsOptional,
    IsISO8601,
    IsString,
    IsNotEmpty,
    IsNotEmptyObject,
    ValidateNested,
} from 'class-validator'

import { BaseLocation } from './cls_location'
import { Type } from 'class-transformer'
import 'reflect-metadata'

export class IssueEvent {

    // ---------- Mandatory
    @IsNotEmpty()
    @IsString()
    originatorName: string
    // example: A Container Moving Enterprise
    // The proper name(human readable) of the entity that published this event.

    // ---------- Mandatory
    @IsNotEmpty()
    @IsString()
    originatorId: string
    // example: ACME
    // The party identification code(i.e.SCAC) of the organization publishing the event

    @IsOptional()
    @IsInt()
    eventSubmissionTime: number // ($int64)
    // -- Because eventSubmissionTime8601 is mandatory ( not both eventSubmissionTime & eventSubmissionTime8601 )
    // example: 1505048400000
    // Time of submission, as the number of milliseconds since Epoch

    // ---------- Mandatory
    @IsNotEmpty()
    @IsISO8601()
    eventSubmissionTime8601: string
    // example: 2017 - 09 - 15T15: 30: 00.000 - 05: 00
    // Time of submission, in common ISO 8601 format yyyy - MM - dd'T'HH: mm: ss.SSSZ

    @IsOptional()
    @IsString()
    eventSubmissionGpsLocation: string
    // example: 41.108708, -73.718832
    // GPS coordinates of where the event originated from(mobile device only)

    @IsOptional()
    @IsString()
    correlationId: string
    // example: C132761278346
    // User - supplied ID for this event

    // ---------- Condition
    @IsOptional()
    @IsString()
    carrierBookingNumber: string
    // example: 123456789
    // The Carrier Booking Number

    // ---------- Condition
    @IsOptional()
    @IsString()
    billOfLadingNumber: string
    // example: BOL3645678
    // The Bill of Lading number

    // ---------- Mandatory
    @IsNotEmpty()
    @IsString()
    equipmentNumber: string
    // example: ACME1122334
    // The identification found on the transport equipment

    @IsOptional()
    @IsString()
    transportEquipmentId: string
    // example: c93b1946 - d8f7 - 11e7 - 9296 - cec278b6b50a
    // Generated ID for the transport equipment.This should be the value returned by the Start equipment tracking event

    @IsOptional()
    @IsString()
    transportEquipmentRef: string
    // example: UniqueTrackingRef1234
    // Reference to an existing transport equipment.Value should be very unique and created by a user

    // ---------- Mandatory
    @IsNotEmpty()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => BaseLocation)
    location: BaseLocation | {}
    // location * BaseLocation{... }

    constructor(json: any) {
        this.originatorName = json.originatorName
        this.originatorId = json.originatorId
        this.eventSubmissionTime = json.eventSubmissionTime
        this.eventSubmissionTime8601 = json.eventSubmissionTime8601
        this.eventSubmissionGpsLocation = json.eventSubmissionGpsLocation
        this.correlationId = json.correlationId
        this.carrierBookingNumber = json.carrierBookingNumber
        this.billOfLadingNumber = json.billOfLadingNumber
        this.equipmentNumber = json.equipmentNumber
        this.transportEquipmentId = json.transportEquipmentId
        this.transportEquipmentRef = json.transportEquipmentRef
        this.location = json.location ? new BaseLocation(json.location) : {}
    }

}