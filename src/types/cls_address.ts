import {
    IsNotEmpty,
    IsOptional,
    IsString,
    IsISO31661Alpha2,
} from 'class-validator'

export class Address {

    @IsOptional()
    @IsString()
    address1: string
    // example: 1234 Tradelens Ave
    // The first line of the street address.

    @IsOptional()
    @IsString()
    address2: string
    // example: Suite 1A
    // The second line of the street address.

    @IsNotEmpty()
    @IsString()
    city: string // mandatory
    // example: Raleigh
    // The name of the city.

    @IsNotEmpty()
    @IsISO31661Alpha2()
    country: string // mandatory
    // example: US
    // The country identification provided in the 2 Alpha ISO 3166 country code standard. The country codes are available in the Platform Constants API

    @IsOptional()
    @IsISO31661Alpha2()
    stateProvince: string
    // example: NC
    // The province or state identification, provided in the ISO 3166-2 standard.

    @IsOptional()
    @IsString()
    zipPostal: string
    // example: 27709-2195
    // The zip or postal code used to identify a geographical area within a country/region.

    constructor(json: any) {
        this.address1 = json.address1
        this.address2 = json.address2
        this.city = json.city
        this.country = json.country
        this.stateProvince = json.stateProvince
        this.zipPostal = json.zipPostal
    }
}