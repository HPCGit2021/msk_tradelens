import {
    IsOptional,
    IsISO8601,
    IsString,
    IsNotEmpty,
} from 'class-validator'

import { IssueEvent } from './issue_event'

export class E292 extends IssueEvent {

    @IsOptional()
    @IsString()
    consignmentId: string
    // example: c93b1946-d8f7-11e7-9296-cec278b6b50a
    // Generated ID for the consignment

    @IsNotEmpty()
    @IsISO8601()
    eventOccurrenceTime8601: string // mandatory
    // example: 2017 - 09 - 15T15: 30: 00.000 - 05: 00
    // When the event occurred, in common ISO 8601 format yyyy - MM - dd'T'HH: mm: ss.SSSZ

    constructor(json: any) {
        super(json)
        this.consignmentId = json.consignmentId
        this.eventOccurrenceTime8601 = json.eventOccurrenceTime8601
    }

}