export enum EVENT {
    E217 = 'E217', 
    E268 = 'E268', 
    E452 = 'E452', 
    E453 = 'E453', 
    // E280 = 'E280', 
    // E025 = 'E025', 
    // E500 = 'E500', 
    // E501 = 'E501', 
    E472 = 'E472', 
    E292 = 'E292', 
    // E425 = 'E425', 
    E429 = 'E429',
    E336 = 'E336',
    E335 = 'E335'
}

export enum TRANSTYPE {
    Import = 'Import', 
    Export = 'Export', 
    Transshipment = 'Transshipment', 
    Domestic = 'Domestic'
}

export enum FULLSTS {
    Full = 'Full', 
    Empty = 'Empty'
}

export enum CUTTYPE {
    Cargo = 'Cargo', 
    Documentation = 'Documentation', 
    VGM = 'VGM'
}

export enum STATUS {
    SUCCESS = 200,
    WARNING = 400,
    ERROR = 500,
    CREATED = 201,
    TOOMANY = 429,
}

export interface message {
    code: STATUS,
    messeage: any
}
