import {
    IsNotEmpty,
    IsOptional,
    IsString,
    MaxLength,
    ValidateNested,
} from 'class-validator'

import { Address } from './cls_address'
import { GeoCoordinates } from './cls_coordinates'

import { Type } from 'class-transformer'
import 'reflect-metadata'

export class BaseLocation {
    // type: string
    // DEPRECATED (DO NOT USE) - The - type - of - location - provided -in -the - value - field
    // Enum:
    // UN/Locode, GLN, Address

    // value: string
    // DEPRECATED(DO NOT USE) - Location - data -for-the - event

    @IsNotEmpty()
    @IsString()
    @MaxLength(5)
    unlocode: string
    // example: USNWK
    // A valid UN / CEFACT value for a UN / Locode

    @IsOptional()
    @ValidateNested()
    @Type(() => Address)
    address: Address | undefined
    // address	Address{... }

    @IsOptional()
    @IsString()
    smdgTerminal: string
    // example: USEWR - PNCT
    // A valid SMDG code for a Ocean Terminal

    @IsOptional()
    @ValidateNested()
    @Type(() => GeoCoordinates)
    geoCoord: GeoCoordinates | undefined
    // geoCoord	GeoCoordinates{... }

    @IsOptional()
    @IsString()
    splc: string
    // example: 191821
    // A valid SPLC code for a Rail Terminal

    @IsOptional()
    @IsString()
    gln: string
    // example: 1234567890123
    // A GS1 Identification Key for the location

    constructor(json: any) {
        this.unlocode = json.unlocode
        this.address = json.address ? new Address(json.address) : undefined
        this.smdgTerminal = json.smdgTerminal
        this.geoCoord = json.geoCoord ? new GeoCoordinates(json.geoCoord) : undefined
        this.splc = json.splc
        this.gln = json.gln
    }
}