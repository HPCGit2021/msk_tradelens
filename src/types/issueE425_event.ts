import {
    IsNotEmpty,
    IsISO8601,
} from 'class-validator'

import { IssueEvent } from './issue_event'

export class E425 extends IssueEvent {

    @IsNotEmpty()
    @IsISO8601()
    eventOccurrenceTime8601: string // mandatory
    // example: 2017 - 09 - 15T15: 30: 00.000 - 05: 00
    // When the event occurred, in common ISO 8601 format yyyy - MM - dd'T'HH: mm: ss.SSSZ

    constructor(json: any) {
        super(json)
        this.eventOccurrenceTime8601 = json.eventOccurrenceTime8601
    }

}