import {
    IsInt,
} from 'class-validator'

export class GeoCoordinates {

    @IsInt()
    latitude: number // ($double) mandatory
    // example: 40.693351
    // Latitude coordinate

    @IsInt()
    longitude: number // ($double) mandatory
    // example: -74.147572
    // Longitude coordinate

    constructor(json: any) {
        this.latitude = json.latitude
        this.longitude = json.longitude
    }
}