import {
    IsNotEmpty,
    IsISO8601,
    IsString,
} from 'class-validator'

import { IssueEvent } from './issue_event'

export class E336 extends IssueEvent {

    @IsNotEmpty()
    @IsISO8601()
    eventOccurrenceTime8601: string
    // example: 2017 - 09 - 15T15: 30: 00.000 - 05: 00
    // When the event occurred, in common ISO 8601 format yyyy - MM - dd'T'HH: mm: ss.SSSZ

    @IsNotEmpty()
    @IsString()
    newSlotNumber: string
    // New Slot Number means the NEW Stowage number

    constructor(json: any) {
        super(json)
        this.eventOccurrenceTime8601 = json.eventOccurrenceTime8601
        this.newSlotNumber = json.newSlotNumber
    }

}