import {
    IsEnum,
    IsOptional,
    IsISO8601,
    IsString,
    IsNotEmpty,
} from 'class-validator'

import { IssueEvent } from './issue_event'
import { CUTTYPE } from './cls_enum'

export class E472 extends IssueEvent {

    @IsOptional()
    @IsString()
    consignmentId: string
    // example: c93b1946-d8f7-11e7-9296-cec278b6b50a
    // Generated ID for the consignment

    @IsOptional()
    @IsISO8601()
    eventOccurrenceTime8601: string
    // example: 2017 - 09 - 15T15: 30: 00.000 - 05: 00
    // When the event occurred, in common ISO 8601 format yyyy - MM - dd'T'HH: mm: ss.SSSZ

    @IsOptional()
    @IsString()
    terminal: string
    // The terminal where the event will occur to satisfy this cut off

    @IsNotEmpty()
    @IsEnum(CUTTYPE)
    cutOffType: CUTTYPE // mandatory
    // Type of cut off

    @IsNotEmpty()
    @IsISO8601()
    cutOffDateTime8601: string // mandatory
    // Deadline for the cut off, in common ISO 8601 format yyyy-MM-dd'T'HH:mm:ss.SSSZ . If possible, this should be specified in the time zone where the event is expected to occur

    @IsOptional()
    @IsString()
    cutOffEventType: string
    // Name of the event endpoint that must be received to satisfy this cut off. As an example, for the cutOffType "VGM", you might set cutOffEventType to "vgmSubmitted"

    constructor(json: any) {
        super(json)
        this.consignmentId = json.consignmentId
        this.eventOccurrenceTime8601 = json.eventOccurrenceTime8601
        this.terminal = json.terminal
        this.cutOffType = json.cutOffType
        this.cutOffDateTime8601 = json.cutOffDateTime8601
        this.cutOffEventType = json.cutOffEventType
    }

}