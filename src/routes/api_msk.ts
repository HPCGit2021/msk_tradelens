import schedule from 'node-schedule'
import moment from 'moment'
import fs from 'fs'

import TIPCController from '../controllers/controller'
import TokenController from '../controllers/token'

import log from '../controllers/logs'
import {folder} from '../config/dev'

let accessToken = ''

const tokenController = new TokenController()
let tipcController: TIPCController
let tokenLog: log = new log('Token')
let resultLog: log = new log('Result')

async function getToken(): Promise<string> {
    accessToken = await tokenController.getAccessToken()
    if (!accessToken) {
        tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get token by access key has failed ...\n`)
        return ''
    }
    tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get token by access has done ...\n`)
    
    return await tokenController.getBearerToken(accessToken)
}

export async function runProcess() {
    try {
        await fs.mkdirSync(folder.f_send, { recursive: true })
        await fs.mkdirSync(folder.f_error, { recursive: true })
        await fs.mkdirSync(folder.f_res, { recursive: true })
    } catch {
        return console.log("Can't find or create folder [out, error, in]")
    }

    // The first token
    let bearerToken = await getToken()
    //if (!bearerToken) console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get token failed ...`)
    //else {
        tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get token has done. Start process ...\n`)
        tipcController = new TIPCController()
        
        // Transport file every 1 minute
        const sendFile = schedule.scheduleJob('*/1 * * * *', async () => {
            
            // Get the token when it is expired
            if (tipcController.error401) {   
                            
                tokenLog.writeLog(`\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get the token when it is expired ...\n`)

                bearerToken = await tokenController.getBearerToken(accessToken)
                if (!bearerToken) {
                    tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get bearer token has failed. Try with access token ...\n`)
                    // Get token by access key
                    bearerToken = await getToken()
                }
                else {
                    tipcController.error401 = false
                    tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get the token successful ...\n`)
                }
            }

            if (!bearerToken) {
                console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Stopped to send files (no token) ...`)
                return tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Stopped to send files (no token) ...\n`)
            }
            
            // Stop schedule
            if(tipcController.lastProcess()) {
                console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ******* Stop schedule *******`)
                tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ******* Stop schedule ******* \n`)
                sendFile.cancel()
            }

            let list = await tipcController.countFiles()
            let len = list.length
            if (len > 0) {
                tipcController.logger.writeLog(`\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] ---------- DIR_LOOP read_cnt: : [${len}]\n`);
                await tipcController.sendFromOneFolder(bearerToken, list)
            } else {
                console.log("No files to process ... ")
            }
        })
    //}
}